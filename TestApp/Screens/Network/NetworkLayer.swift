//
//  NetworkLayer.swift
//  TestApp
//
//  Created by Shahrukh on 05/04/2023.
//

import Foundation

protocol APIS {
    
}

class Network {
    
    static let shared = Network()
    private init() {}
    
    func getKeys (completion: @escaping (_ data: HomeModel) -> ()) {
        
        // Create a URL object from the API endpoint
        let url = URL(string: "https://mocki.io/v1/b8853964-ea72-47a7-8310-b05f4abc06a5")!//"https://mocki.io/v1/ae0a0556-c284-4c16-a00c-a42722c32a57")!

        // Create a URLSession object
        let session = URLSession.shared

        // Create a data task to send the HTTP request and receive the response
        let task = session.dataTask(with: url) { data, response, error in
            // Handle any errors
            if let error = error {
                print("Error: \(error.localizedDescription)")
                return
            }

            // Check the response status code
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                print("Error: Invalid HTTP response")
                return
            }

            // Unwrap the received data
            guard let responseData = data else {
                print("Error: No data received")
                return
            }

            // Decode the JSON data into a Swift object
            do {
                let decodedData = try JSONDecoder().decode(HomeModel.self, from: responseData)
                // Process the decoded data
                completion(decodedData)
            } catch let error {
                print("Error: \(error.localizedDescription)")
                return
            }
        }

        // Start the data task
        task.resume()
    }
}
