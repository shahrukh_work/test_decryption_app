//
//  ViewController.swift
//  TestApp
//
//  Created by Shahrukh on 04/04/2023.
//
import UIKit
import SwiftyRSA

class ViewController: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var informationLabel: UILabel!
    var interacter = Interactor()

    
    //MARK: - Variables
    
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    
    //MARK: - Setup
    func setup () {
        loadData()
    }
    
    
    //MARK: - Selectors
    
    
    //MARK: - Actions
    
    
    //MARK: - Private Methods
    private func loadData () {
        interacter.getEncrptedData {
            DispatchQueue.main.async {
                self.informationLabel.text = self.interacter.presentableText
            }
        }
    }
}

