//
//  Interactor.swift
//  TestApp
//
//  Created by Shahrukh on 05/04/2023.
//

import Foundation
import SwiftyRSA

protocol RSADecryption {
    func getEncrptedData (completion: @escaping () -> ())
    func decryptData (homeModel: HomeModel, completion: @escaping (String) -> ())
}


class Interactor: RSADecryption {
    
    var presenter = Presenter()
    var presentableText = ""
    
    
    /// This will decrypt the encrypted data `subject`.
    ///
    /// - Parameters:
    ///     - subject: HomeModel, callback
    ///
    /// - Returns: nothing `subject`.
    func decryptData(homeModel: HomeModel, completion: @escaping (String) -> ()) {
        let privateKey = try! PrivateKey(base64Encoded: homeModel.key)
        let encrypted = try! EncryptedMessage(base64Encoded: homeModel.text)
        let clear = try! encrypted.decrypted(with: privateKey, padding: .PKCS1)

        // Then you can use:
        let data = clear.data
        let base64String = clear.base64String
        let string = try! clear.string(encoding: .utf8)
        presentableText = presenter.beautify(text: string)
        completion(presentableText)
    }
    
    /// API Call to get the encrypted data`subject`.
    ///
    /// - Parameters:
    ///     - subject: callback
    ///
    /// - Returns: nothing `subject`.
    func getEncrptedData (completion: @escaping () -> ()) {
        
        Network.shared.getKeys { [self] data in
            
            self.decryptData(homeModel: data) {_ in
                completion()
            }
        }
    }
}
