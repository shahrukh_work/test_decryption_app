//
//  Presenter.swift
//  TestApp
//
//  Created by Shahrukh on 05/04/2023.
//

import Foundation
import ObjectMapper

protocol HomePresenter {
    func beautify (text: String) -> String
}

class Presenter: HomePresenter {
    
    
    /// Improve the visualization of for UI `subject`.
    ///
    /// - Parameters:
    ///     - subject: improve data presentation
    ///
    /// - Returns: readable text `subject`.
    func beautify(text: String) -> String {
        
        let object = Mapper<DecryptedModel>().map(JSONString: text)
        return "prodURL is: \(object?.prodBaseURL ?? "") \n devURL is: \(object?.devBaseURL ?? "") \n stag is: \(object?.stagBaseURL ?? "")"
    }
}
