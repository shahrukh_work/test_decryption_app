//
//  HomeModel.swift
//  TestApp
//
//  Created by Shahrukh on 05/04/2023.
//

import Foundation
import ObjectMapper

class HomeModel: Codable {
    var key : String = ""
    var text : String = ""
}


class DecryptedModel: Mappable {
    
    var prodBaseURL: String?
    var stagBaseURL: String?
    var devBaseURL: String?
    var uatBaseURL: String?
    var apiKey: String?

    required init?(map:Map) {
    }
    
    func mapping(map: Map) {
        prodBaseURL <- map["prodBaseURL"]
        stagBaseURL <- map["stagBaseURL"]
        devBaseURL <- map["devBaseURL"]
        uatBaseURL <- map["uatBaseURL"]
    }
}
