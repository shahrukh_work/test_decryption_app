# test_decryption_app



## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/shahrukh_work/test_decryption_app.git
git branch -M main
git push -uf origin main
git checkout dev
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/shahrukh_work/test_decryption_app/-/settings/integrations)


***


## Name
TestApp

## Description
This is the demo task to make sure the baseURL and endpoints are secure even if the reverse engineering is applied. This app does not contain an amazing UI. It contains only single screen and single label.

## Architecture Used
VIPER architecture/design pattern is used in this app. 


## Frameworks
- ObjectMapper
- SwiftyRSA


## How
Steps:
[1] Used online RSA encryption using this website. https://www.devglan.com/online-tools/rsa-encryption-decryption
*This website converted the baseURL Json to base64 encrypted string.*

[2] Used this fake api https://mocki.io/fake-json-api since there is not server available. This link contains the encrypted json string values.

[3] After getting the data from the above link then used this data in RSA decryption algorithm to get the original data. After that this data can be stored locally in order to use it next time.
