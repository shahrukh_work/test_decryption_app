//
//  TestAppTests.swift
//  TestAppTests
//
//  Created by Shahrukh on 04/04/2023.
//

import XCTest
import ObjectMapper
@testable import TestApp

final class TestAppTests: XCTestCase {
    
    func testDecryption () {
        let value = HomeModel()
        value.text = "Ns1g1qWxgSHQ/3s78Snaf81jwxUvSe5WBEtepi3Lcblml/U/+o51m1BW+1kJAHy16m7ytUF8uPqRRmz0Sy2mILQv6FEk1WOk8iXuO1oCsV89svFpexSrhX3jtY32oNexNu771AgXydKNk+W80ChznKxizQr5acWoix0iYR5U6gu92opUk1QzwTeod657k9ZY4Xglcyi2gsx1LMY/JJtwyZSsjBhzIoOY+fbwIOzGuo6bb4jivYXr1GM2OF9L3uyapAY+oPwCEkYbsa98gTEw56Or/FjLoj+IkKh8NwVBMSAcbsOyP4BPzobJMIJJTYNCkEzwqLvX4afcxmcHIbjgIQ=="
        value.key = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCJEvOwxC9wzlIdznX9pM5JyMWeuP8Kd+28/HwT/urEWjRiFMVJdX8lUOlko2uqMKDnDINt5wuFdas1yFcs3xysH1z56JoeoO4GaCYciWfUfrTp5rJYhnFMnDTKLfi+/mYEOLCX7rFgRfhOGuO1D27FB2P2eqEH670RcqGj2UaFve0k1VGarZMFk8N7yNVok+bAvl60VbYGl2n7VbaQft7dAe4MBhu4vZy9IVg4lmKZntIGwnXEzASzGClq+fbr1oNB7eApwJp8fCJhmiwVEbLbqXqg/MAjXMbiDTxvhIMChxlY9vxLAs/pkKa5Hi+OPGapPaFhyVfXG7Qp+rMZ3eP7AgMBAAECggEAGeGALYH9SxlJm+MdmBSD9p534a4gSYO3WZV6f/QDs6ssdNBjJ6mNpigOyTep3mpsFvPf5OJY5IwBFZiMDSbbLQyG65CleN790ivb9ktjVVXPJtKuHdTUk5hyxzHC45z4+hcSEL26s+jCJ00gbVFKotzwXNtuiCZUfnTcXClsDVg0ot9NKT157y/Sz2V0cWHMl1LuH9SFSLvZC7ADbWu4VcCjszOXlJ8Em61osdPhnbq8B0lIF4Vh7WfAnv7KDBAP7jDZgqa+SR3Vaa+GyPHY47VKP4jHlFgn06NEjZ9nlPvEkX6gLaWfaSHcHzQ7nO+p47OGJbvGNLEmaW2BetapoQKBgQDm1Sec23ufcIqNhzuGv2E60gJESnG8LlEOCSDM0BbAkXXZytJUYp5tzylgHHjRyTSV2R8uNbGNn3vArOE6ZjjJCDmw9VisaOSCKul9T1/sMT+xjV/5CjLcrnjktBvqqHHrIdssKTcqNN2/kgj5hE+Le2BJqId8t4VHy6cbat8+EwKBgQCYBN68uK+MSo/vQgu3szTIS0OKsf9YMzLAQPd8hx35xSJfO7a4j3VDesZEgflUs4bKTuC8w39lL3bhLP/Y45TKRkQuzR+f9aIAIrobYJje1s6fT2GLSQBJNSEP7KXusqSk5mYM3jCAsvnhS+avtF9UWCoEfQOLFyj1RfUG8H3feQKBgEIvZd9E00pg82knGDtHjnJWGs5H5hqF0TnMBjqTg6KNUpQy4mHNsQ1dM8jaAQ5tuYUprxYJ0TIBQ3N4Xk/nVWrWz+vcPSOaI8Lq2O3UH3XSnSekmJsIgJLj98fjmImM6LT5jSU5YBba96XhEm+wrzoYEwCrsMVivXVbgDuNRhs/AoGASm0n99Gb8N2YdpUpU3jjgKSInuDimPYIBORJxqgIXv7LA0EjBl0GFlGHvIa94WA7v9S6ZvB38Lw6QahcYn2ZnSalvxwiPZhvBg36e+jhL0iPxKOCqETHZRAj7b34x1KjFTDQCcDGklwNxuX4PVHb0lmPyL7D+1cNcHgdrJYVqlECgYAhunq7FY4JTSFUSs/arprfq7VRmxIOA95ZQf72z81PvNMboMUlKj8VWsyweMj+NM2x/EwjAbEAqxPF2cFMQ0eXxjY8Lfzqe6C5j3DhKdfT/AL+7ESAnLgcsYBatveMP1GHY6n9SAlm5RiJ6lDSxfb2zTTw0sfiibjn25giYzTghw=="
        
        let interactor = Interactor()
        
        interactor.decryptData(homeModel: value) { result in
            XCTAssertTrue(!result.isEmpty)
        }
        
    }
}
